section .text

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

string_length:
    xor rax, rax

.loop:
    cmp byte[rdi+rax], 0
    je .end
    
    inc rax
    
    jmp .loop

.end:
    ret
    
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi

    xor rax, rax
    call string_length
    mov rdi, 1   
    pop rsi      
    mov rdx, rax  
    mov rax, 1
    syscall

    ret
    
    
    
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

    
; Принимает код символа и выводит его в stdout
print_char:
    sub rsp, 1
    mov [rsp], dil
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
        
    add rsp, 1    
    ret
    




; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:

    push rbx
    push rsi
    sub rsp, 32          
    mov rsi, rsp         

    mov rbx, 10          
    mov rax, rdi         
    
    test rax, rax        
    jz .handle_zero

.loop:
    xor rdx, rdx         
    div rbx              
    add dl, '0'          
    dec rsi              
    mov [rsi], dl        

    test rax, rax        
    jnz .loop
    jmp .write_output

.handle_zero:
    dec rsi              
    mov byte [rsi], '0'  

.write_output:
    mov rax, 1           
    mov rdi, 1           
    mov rdx, rsp         
    sub rdx, rsi
    syscall              

    add rsp, 32     
    pop rsi
    pop rbx     
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 

print_int:
    test rdi, rdi                ; Проверяем, положительное число или отрицательное
    jns .positive                ; Если не отрицательное, переходим к .positive
    
    push rdi
    mov dil, '-'
    call print_char
    pop rdi
    neg rdi  
                                ; Изменяем знак числа на положительный
.positive:
    jmp print_uint              ; Вызываем функцию print_uint для вывода беззнакового числа
    


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	

.loop:
    
    mov al, byte [rdi]
    cmp al, byte [rsi]
    
    
    jne .end

    
    test al, al
    jz .strings_are_equal
    
    
    inc rdi
    inc rsi

    
    jmp .loop

.strings_are_equal:
    mov rax, 1
    ret

.end:
    xor rax, rax
    ret
    




; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    
    sub rsp, 1

    
    mov rax, 0      
    mov rdi, 0      
    mov rsi, rsp    
    mov rdx, 1      

    
    syscall

   
    test rax, rax   
    jz .eof
    mov al, [rsp]
    add rsp, 1
    ret
.eof:
    
    add rsp, 1

    xor rax, rax 
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    
    mov r12, r8
    mov r13, rdi
    mov r14, rsi  

.loop_check:
    call read_char  
    
    cmp al, 0x20
    je .skip_whitespace
    
    cmp al, 0x9
    je .skip_whitespace
    
    cmp al, 0xA
    je .skip_whitespace
    
    test al, al  
    jz .exit_success

    
    cmp r12, r14
    jae .exit_fail
    mov [r13 + r12], al  
    inc r12

    jmp .loop_check  

.exit_success:
    mov byte [r13 + r12], 0  
    mov rax, r13
    mov rdx, r12
    jmp .end

.exit_fail:
    xor rax, rax  
    
.end:
    pop r14
    pop r13
    pop r12 
    ret

.skip_whitespace:
    test r12, r12
    jnz .exit_success
    jmp .loop_check

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    
    .loop:
    	movzx r8, byte [rdi + rdx]
    	test r8, r8
    	jz .end
    	sub r8, 0x30
    	cmp r8, 9
    	ja .end
    	
    	imul rax, rax, 10
    	add rax, r8
    	inc rdx
    	
    	jmp .loop
    	
    .end:
    	test rdx, rdx
    	jz .fail
    	ret
    .fail:
    	xor rax, rax
    	xor rdx, rdx
    	ret
	
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, [rdi]
    cmp al, '-'      
    jne .positive              
    
    jmp .handle_sign
    
.positive:
    cmp al, '+'
    je .handle_sign
    call parse_uint 
    
.end:          
    ret                       

.handle_sign:
    inc rdi                    
    call parse_uint            
    
    test rdx, rdx                 
    je .fail                   
    
    inc rdx
    
    cmp al, '+'
    je .end
    
    neg rax
    jmp .end
    
    
.fail:
    xor rax, rax                             
    ret
  
    	
    
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
xor rcx, rcx 

    .main_loop:
       
        cmp rcx, rdx
        jae .failure 
        
       
        mov al, [rdi + rcx]
        mov [rsi + rcx], al
        
        
        test al, al
        jz .success_exit
        
       
        inc rcx

        
        jmp .main_loop

    .success_exit:

        mov rax, rcx
        ret

    .failure:
        
        xor rax, rax
        ret
